package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
@SpringBootTest
class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_and_content_type_is_text_plain_when_match_user_id() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/2/books"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.content().string("The book for user 2"));
    }

    @Test
    void should_case_sensitive_and_return_5xx_request() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/case/sensitive/2"))
                .andExpect(MockMvcResultMatchers.status().is5xxServerError());
    }

    @Test
    void should_match_the_more_accurate_api_rather_than_less_accurate() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/segments/good"))
                .andExpect(MockMvcResultMatchers.content().string("more accurate"));
    }

    @Test
    void should_not_match_several_word_when_using_single_word_wildcards() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/singleWildcards/222"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void should_match_wildcards_in_url_end() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcards/anything"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("success"));
    }

    @Test
    void should_match_wildcards_in_url_middle() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/anything/wildcards"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("success"));
    }


    @Test
    void should_not_match_wildcards_is_not_right_position() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/before/after"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void should_match_prefix_and_suffix_wildcards() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/prefix/wildcards/suffix"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("success"));
    }

    @Test
    void should_match_multicore_wildcards_in_middle() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/anything/suffix"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void should_match_regular_expression() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/goods/199}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("success"));
    }

    @Test
    void should_match_query_string() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/books?year=1996"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("success"));
    }

    @Test
    void should_return_bad_request_when_not_given_request_Param() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/books"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void should_return_200_when_filter_a_request_param() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/author"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
