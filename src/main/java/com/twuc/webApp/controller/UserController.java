package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {
    @GetMapping("/users/{userId}/books")
    private String getUserBooks(@PathVariable int userId) {
        return "The book for user " + userId;
    }

    @GetMapping("/case/sensitive/{userId}")
    private String caseSensitive(@PathVariable int userid) {
        return "case sensitive";
    }

    @GetMapping("/segments/good")
    private String moreAccurate() {
        return "more accurate";
    }

    @GetMapping("/segments{segmentName}}")
    private String lessAccurate(@PathVariable String segmentName) {
        return "less accurate";
    }

    @GetMapping("/singleWildcards/?")
    private String wildcardsMatch() {
        return "can not match";
    }

    @GetMapping("/wildcards/*")
    private String matchEndWildcards() {
        return "success";
    }

    @GetMapping("/*/wildcards")
    private String matchMiddleWildcards() {
        return "success";
    }

    @GetMapping("/wildcard/before/*/after")
    private String matchNotRightPositionWildcards() {
        return "success";
    }


    @GetMapping("/prefix/wildcards/suffix")
    private String matchPrefixAndSuffix() {
        return "success";
    }

    @GetMapping("/**/suffix")
    private String matchMulticoreWildcards() {
        return "success";
    }

    @GetMapping("/g{o+}ds/{%d+}")
    private String matchRegularExpression() {
        return "success";
    }

    @GetMapping("/books")
    private String matchQueryString(@RequestParam int year) {
        return "success";
    }

    @GetMapping("/author")
    private String matchFilterQueryString(@RequestParam(required = false)String birthday) {
        return "success";
    }
}
